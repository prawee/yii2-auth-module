<?php
/*
 * 2014-07-21
 * prawee@hotmail.com
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use kartik\icons\Icon;

Icon::map($this);

$this->title = 'Change Password';

Modal::begin([
    'id' => 'form-modal',
    'closeButton' =>[],
    'header' => '<b>' . Icon::show('refresh') . 'Change Password</b>']);
?>
<div class="change-password">
    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'change-password-form']); ?>
            <?= $form->field($model, 'oldPassword')->passwordInput() ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'repeatPassword')->passwordInput() ?>
            <div class="form-group">
                <?= Html::submitButton(Icon::show('edit') . ' Save', ['class' => 'btn btn-primary', 'name' => 'change-password-button']) ?>
                <?= Html::a(Icon::show('times') . ' Cancel',['/auth/user/profile'], ['class' => 'btn btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
Modal::end()?>
