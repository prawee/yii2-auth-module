<?php
/*
 * 2014-06-25
 * prawee@hotmail.com
 * example output
 * [
        'Role'=>[
            'administrator'=>'Administrator',
            'partner'=>'Partner',
            'staff'=>'Staff'
        ],
        'Administrator' => [
            'gameManage' => 'Game Manage',
            'newsManage'=> 'News Manage'
        ],
        'Partner'=>[
            'newsManage'=> 'News Manage'
        ],
        'Staff'=>[
            'newsManage'=> 'News Manage'
        ]
    ]
 */
namespace auth\models;

use Yii;
//use yii\base\Model;
//use yii\helpers\VarDumper;
//use yii\helpers\Html;
use common\models\AuthItem as CAuthItem;

class AuthItem extends CAuthItem{
    
    public static function getItem(){
        
        $item1=[];
        $item2=[];
        $auth=Yii::$app->authManager;
        $auth->init();
        $roles=$auth->getRoles();
        
        foreach($roles as $role){
            $item1[$role->name]=$role->description;
            
            $permissions=$auth->getPermissionsByRole($role->name);
            $data=[];
            foreach($permissions as $permission){
                $data[$permission->name]=$permission->description;
            }
            $item2[$role->description]=$data;
        }
        return [
            'Roles'=>$item1,
            $item2,
        ];
    }
    public function getDescription(){
        switch($this->type){
            case '1': $type='Role'; break;
            case '2': $type="Permission"; break;
            case '3': $type='Role(Private)'; break;
            case '4': $type='Permission(Private)'; break;
        }
        return $this->description.' ('.$type.')';
    }
}
