<?php

/*
 * 2014-06-20
 * created by prawee@hotmail.com
 */

namespace auth\controllers;

use Yii;
use yii\web\Controller;
use admin\app\models\UserSearch;
use auth\models\User;
use auth\models\AssignmentForm;
use auth\components\AccessControl;
use auth\Asset;
use auth\models\ResetForm;

class UserController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
        ];
    }
    public function init()
    {
        parent::init();
        Asset::register($this->view);
    }

    public function actionIndex() {
        $searchModel = new UserSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider
        ]);
    }

    public function actionPermission($id) {
        $user = User::findOne($id);
        $model = new AssignmentForm();

        if (isset($user->id)):
            $js = '$("#permission-modal").modal({backdrop:"static",keyboard:false});';
            $this->getView()->registerJs($js);
            return $this->render('permission', [
                        'user' => $user,
                        'model' => $model,
            ]);
        else:
            $this->redirect(['/auth/user']);
        endif;
    }

    public function actionAssignment() {
        $key=Yii::$app->getRequest()->get('key');
        $type=Yii::$app->getRequest()->get('type');
        $userId=Yii::$app->getRequest()->get('userid');

        $auth = Yii::$app->authManager;
        $auth->init();

        $role = $auth->getRole($key);
        $permission = $auth->getPermission($key);

        $item = is_object($role) ? $role : $permission;

        if (is_object($item) && isset($userId)) {
            if ($type === 'true'):
                //Yii::$app->getSession()->setFlash('info-modal','Adding '.$item->name);
                $chkAuth = $auth->getAssignment($item->name, $userId);
                //!isset($chkAuth->roleName)?$auth->assign($item,$userId):'';
                if (!isset($chkAuth->roleName)):
                    Yii::$app->getSession()->setFlash('success-modal', 'Added ' . $item->name . ' Completed.');
                    $auth->assign($item, $userId);
                else:
                    Yii::$app->getSession()->setFlash('info-modal', 'Exist data.');
                endif;
            else:
                Yii::$app->getSession()->setFlash('error-modal', 'Deleted ' . $item->name . ' Already!');
                $auth->revoke($item, $userId);
            endif;
        }
    }
    /*
     * 2014-07-20
     */
    public function actionProfile(){
        $id=Yii::$app->user->identity->id;
        $model= User::findOne($id);
        return $this->render('profile',[
            'model'=>$model,
        ]);
    }
    /*
     * 2014-07-21
     */
    public function actionChangePassword(){
        $model = new ResetForm();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->resetPassword()) {
            return $this->redirect(['/auth/user/profile']);
        }
        return $this->render('change-password',[
            'model'=>$model,
        ]);
    }
    /*
     * 20140722
     */
    public function actionUpdateProfile(){
        $model=  User::findIdentity(Yii::$app->user->identity->id);
        if($model->load(Yii::$app->getRequest()->post()) && $model->validate()){
            if ($model->save()):
                Yii::$app->getSession()->setFlash('success', 'Update your profile finished.');
                return $this->redirect(['/auth/user/profile']);
            endif;
        }
        return $this->render('update-profile',[
            'model'=>$model,
        ]);
    }
    public function actionCreate() {
        $model = new User();
        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }
    public function actionDelete($id){
        if($id!==1){
            $this->findModel($id)->delete();
        }
        return $this->redirect(['index']);
    }
    
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
