<?php

/*
 * 2014-06-25
 * prawee@hotmail.com
 */

namespace auth\components;

//use yii\web\IdentityInterface;
use yii\web\User as BaseUser;
use yii\db\Expression;

class User extends BaseUser {

    /**
     * @inheritdoc
     */
    public $identityClass = '\auth\models\User';

    /**
     * @inheritdoc
     */
    public $enableAutoLogin = true;

    /**
     * @inheritdoc
     */
    public $loginUrl = ['/auth/default/login'];

    public $fieldUpdateAfterLogin='last_login';

    /**
     * @inheritdoc
     */
    protected function afterLogin($identity, $cookieBased, $duration) {      
        $this->identity->setScenario(self::EVENT_AFTER_LOGIN);
        $this->identity->setAttribute($this->fieldUpdateAfterLogin, new Expression('CURRENT_TIMESTAMP'));
        // $this->identity->setAttribute('login_ip', ip2long(\Yii::$app->getRequest()->getUserIP()));
        $this->identity->save(false);
        return parent::afterLogin($identity, $cookieBased, $duration);
    }

    public function getIsSuperAdmin() {
        if ($this->isGuest) {
            return false;
        }
        //call to function => auth\models\User
        return $this->identity->getIsSuperAdmin();
    }

    public function checkAccess($operation, $params = [], $allowCaching = true) {
        // Always return true when SuperAdmin user
        if ($this->getIsSuperAdmin()) {
            return true;
        }
        return parent::can($operation, $params, $allowCaching);
    }

}
