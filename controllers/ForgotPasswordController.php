<?php
namespace auth\controllers;

use Yii;
use yii\web\Controller;
use auth\models\User;
use yii\helpers\Security;
use auth\components\AccessControl;

class ForgotPasswordController extends Controller{
    public function actions()
    {
        return [
            'access'=>[
                'class'=>  AccessControl::className(),
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex() {
        $model =new User;
        $model->setScenario('requestPasswordResetToken');
        if ($model->load($_POST) && $model->validate()) {
            if ($this->sendPasswordResetEmail($model->email)) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
                //return $this->goHome();
                return Yii::$app->getResponse()->redirect(['/auth/forgot-password/notification']);
            } else {
                Yii::$app->getSession()->setFlash('error', 'There was an error sending email.');
            }
        }
        return $this->render('index',[
            'model'=>$model,
        ]);
    }
    private function sendPasswordResetEmail($email) {
        $user = User::findOne([
                    'status' => User::STATUS_ACTIVE,
                    'email' => $email,
        ]);
        if(!$user){
            return false;
        }

        $user->password_reset_token =Security::generateRandomKey();
        if ($user->save(false)) {
            $fromEmail = \Yii::$app->params['supportEmail'];
            $name = '=?UTF-8?B?' . base64_encode(\Yii::$app->name . ' robot') . '?=';
            $subject = '=?UTF-8?B?' . base64_encode('Password reset for ' . \Yii::$app->name) . '?=';
//            $body = $this->renderPartial('/emails/passwordResetToken', [
//                'user' => $user,
//            ]);
            $body='user='.$user->username; 
            $headers = "From: $name <{$fromEmail}>\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";
            return mail($email, $subject, $body, $headers);
        }
        return false;
    }

    public function actionNotification(){
        return $this->render('notification');
    }

}

