<?php
/*
 * 2014-06-24
 * prawee@hotmail.com
 */
namespace auth\controllers;

use Yii;
use yii\web\Controller;
use auth\Asset;
use auth\models\AuthItemForm;
//use auth\components\AccessControl;
use auth\models\RbacForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\db\Query;
use auth\models\AuthItemChild;

class PermissionController extends Controller {

    /*public function behaviors() {
        return [
            'access'=>[
                'class'=>  AccessControl::className(),
            ]
        ];
    }*/
    public function init()
    {
        parent::init();
        Asset::register($this->view);
    }
    public function actionIndex() {
        $auth=Yii::$app->authManager;
        $auth->init();
        
        //Asset::register($this->view);
        //$js = '$("#permission-management-modal").modal({backdrop:"static",keyboard:false});';
        //$this->getView()->registerJs($js);
        
        
        return $this->render('index', [
            'auth'=>$auth
        ]);
    }
   
    public function actionCreate(){
        $save=false;
        $auth=Yii::$app->authManager;
        $model=new AuthItemForm;
        if($model->load(Yii::$app->getRequest()->post()) && $model->validate()){
            $model->attributes=Yii::$app->getRequest()->post('AuthItemForm');

            if($model->type==2):
                $parent=$auth->getPermission($model->parent);
            
                $permission=$auth->createPermission($model->name);
                $permission->description=  is_object($parent)?'- '.$model->description:$model->description;

                if($auth->add($permission)):
                    $save=true;
                    Yii::$app->getSession()->setFlash('success-modal','Adding Permission '.$model->name.' Completed.');
                endif;
                
                if(is_object($parent)):
                    $auth->addChild($parent,$permission);
                endif;
                
            else:
                $role=$auth->createRole($model->name);
                $role->description=$model->description;
    
                if($auth->add($role)):
                    $save=true;
                    Yii::$app->getSession()->setFlash('success-modal','Adding Role '.$model->name.' Completed.');
                endif;
                
                $parent= $auth->getRole($model->parent);
                if(is_object($parent)):
                    $auth->addChild($parent,$role);
                endif;
            endif;
            
            if ($save==false):
                Yii::$app->getSession()->setFlash('error-modal','Something error!.');
                echo Html::script("$('#form-modal').modal('hide');");
                return $this->renderAjax('_formItem', [
                            'model' => $model,
                ]);
            else:
                $this->redirect(['/auth/permission']);
            endif;
        }
        
        return $this->render('create',[
            'model'=>$model
        ]);
    }
    
    public function actionUpdate(){
        $auth = \Yii::$app->authManager;
        $model = new RbacForm(); 
        
        if ($model->load(\Yii::$app->getRequest()->post()) && $model->validate()) {
            $parent = $auth->getRole($model->parent);
            if ($model->type == 1):
                Yii::$app->session->setFlash('info','Updating Role '.$model->name.' Already.');
                $role = $auth->createRole($model->name);
                $role->description = $model->description;
                $auth->add($role);
                $auth->addChild($parent, $role);
            else:
                Yii::$app->session->setFlash('info','Updating Permission '.$model->name.' Already.');
                $permission = $auth->createPermission($model->name);
                $permission->description = $model->description;
                $auth->add($permission);
                $auth->addChild($parent, $permission);
            endif;
            
            $success = $this->renderPartial('success', [
                'model' => $model,
                'auth' => $auth
            ],false);
            echo Html::script("$('#response').html('{$success}'); alertClose(); $('.modal').modal('hide');");
            return $this->renderAjax('form', [
                'model' => $model,
                'auth' => $auth,
            ]);
        }
        
        return $this->render('index1',[
            'auth'=>$auth,
            'model'=>$model,
        ]);
    }
    
    public function actionItems(){
        $auth=Yii::$app->authManager;
        $out = [];
        if (Yii::$app->getRequest()->post('depdrop_parents')) {
            $typeId=end(Yii::$app->getRequest()->post('depdrop_parents'));

            $db=new Query;
            $datas=$db->select('name,description')
                    ->where(['type'=>$typeId])
                    ->from($auth->itemTable)
                    ->orderBy('name')
                    ->all();
            foreach($datas as $data){
                //echo '<pre>'.print_r($data,true).'</pre>';
                
                if($typeId==2){
//                    $subdatas=$auth->getChildren($data['name']);
//                    print_r($subdatas);
//                    $out[$data['description']]=[
//                            ['id'=>1,'name'=>'test1'],
//                            ['id'=>2,'name'=>'test2']
//                    ];
                    $out[]=[
                        'id'=>$data['name'],
                        'name'=>$data['description']
                    ];
                }else{
                    $out[]=[
                        'id'=>$data['name'],
                        'name'=>$data['description']
                    ];
                }
            }
            
            /*$out=[
                'group1'=>[
                   [
                       'id'=>1,
                       'name'=>'test1'
                   ],[
                       'id'=>2,
                       'name'=>'test2'
                   ]
                ],
                'group2'=>[
                    [
                        'id'=>3,
                        'name'=>'test3'
                    ]
                ],
                'group3'=>[
                    [
                        'id'=>'group31',
                        'name'=>'test31'
                    ]
                ]
            ];*/
            
            

//            for($i=0;$i<=5;$i++){
//                $out[$i]=['id'=>($i+1),'name'=>'name'.$i];
//            }
//            $selected=3;
//            echo Json::encode(['output' => $out, 'selected'=>$selected]);
            echo Json::encode(['output'=>$out]);
            return;
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }
    /*
     * 2014-07-16
     */
    public function actionRelations(){
        $auth=Yii::$app->authManager;
        $model=new AuthItemChild;
         if ($model->load(\Yii::$app->getRequest()->post()) && $model->validate()) {
             if(is_object($auth->getRole($model->parent))):
                 $parent=$auth->getRole($model->parent);
             else:
                 $parent=$auth->getPermission($model->parent);
             endif;
             
             /*if(is_object($auth->getRole($model->child))):
                 $child=$auth->getRole($model->child);
             else:
                 $child=$auth->getPermission($model->child);
             endif;*/
             
             //right side for permission only
             /*if(is_object($auth->getPermission($model->child))):
                 $child=$auth->getPermission($model->child);
             else:
                 $child=null;
             endif;*/

             //right side for permission first and role after
             if(is_object($auth->getPermission($model->child))):
                 $child=$auth->getPermission($model->child);
             else:
                 if(is_object($auth->getRole($model->parent))){
                     $child=$auth->getRole($model->child);
                 }else{
                     $child=null;
                 }
             endif;
             
             if($auth->hasChild($parent, $child)):
                Yii::$app->getSession()->setFlash('info-modal',"Exist this child!.");
                return $this->renderAjax('_formRelation', [
                            'model' => $model,
                ]);
             endif;
             
             if (is_object($child) && $auth->addChild($parent,$child)):  
                $this->redirect(['/auth/permission']);
            else:
                Yii::$app->getSession()->setFlash('info-modal',"Can't add this child!.");
                return $this->renderAjax('_formRelation', [
                            'model' => $model,
                ]);
            endif;
         }
        return $this->render('relations',[
            'model'=>$model
        ]);
    }
}
