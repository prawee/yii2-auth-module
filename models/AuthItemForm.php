<?php
/*
 * 2014-06-25
 * prawee@hotmail.com
 */
namespace auth\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
//use yii\base\Model;
//class AuthItemForm extends Model{
class AuthItemForm extends ActiveRecord{
    public $parent;
    //public $name;
    //public $description;
    
    public static function tableName() {
        return Yii::$app->authManager->itemTable;
    }
    
    public function rules(){
        return [
            [['type','name','description'],'required'],
            ['parent', 'exist', 'targetAttribute' => 'name'],
            ['name', 'unique'],
        ];
    }
    public function attributeLabels() {
        return [
            'parent'=>'Parent',
            'name'=>'Name',
            'description'=>'Description',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        $db = new Query();
        $parent = $db
                ->select('parent')
                ->where(['child' => $this->name])
                ->from(\Yii::$app->authManager->itemChildTable)
                ->scalar();
        if ($parent) {
            $this->parent = $parent;
        }
    }
}
