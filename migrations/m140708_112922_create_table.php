<?php

use yii\db\Schema;
use yii\db\Migration;
/*
 * @auth prawee wongsa <prawee@hotmail.com
 */

class m140708_112922_create_table extends Migration {

    public function up() {
        
        //options
        switch(Yii::$app->db->driverName){
            case 'mysql':
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
                break;
            case 'pgsql':
                $tableOptions = null;
                break;
            default:
                throw new RuntimeException('Your database is not supported!');     
        }
        
        //user_type
        $this->createTable('user_type',[
            'id'=>  Schema::TYPE_PK,
            'name'=>  Schema::TYPE_STRING.'(128) NOT NULL'
        ],$tableOptions);
        $this->createIndex('usertype_unique_name','user_type','name',true);
        
        
        //user
        $this->createTable('user',[
            'id'=>  Schema::TYPE_PK,
            'username'=>Schema::TYPE_STRING.'(255) NOT NULL',
            'auth_key'=>Schema::TYPE_STRING.'(32) NOT NULL',
            'password_hash'=>Schema::TYPE_STRING.'(255) NOT NULL',
            'password_reset_token'=>Schema::TYPE_STRING.'(255) NULL',
            'email'=>Schema::TYPE_STRING.'(255) NOT NULL',
            'role'=>Schema::TYPE_SMALLINT.'(6)',
            'status'=>Schema::TYPE_SMALLINT.'(6)',
            'created'=>Schema::TYPE_DATETIME,
            'modified'=>Schema::TYPE_DATETIME,
            'last_login'=>Schema::TYPE_DATETIME,
            'user_type_id'=>Schema::TYPE_INTEGER
        ],$tableOptions);
        $this->createIndex('user_unique_username', 'user', 'username', true);
        $this->createIndex('user_unique_email', 'user', 'email', true);
        $this->addForeignKey('fk_user_usertype','user','user_type_id','user_type','id','SET NULL','CASCADE');
        
        //insert user_type
        $this->insert('user_type',[
            'name'=>'normal',
        ]);
              
        //account
        $this->createTable('account', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER,
            'provider' => Schema::TYPE_STRING . ' NOT NULL',
            'client_id' => Schema::TYPE_STRING . ' NOT NULL',
            'properties' => Schema::TYPE_TEXT
        ], $tableOptions);

        $this->createIndex('account_unique', 'account', ['provider', 'client_id'], true);
        $this->addForeignKey('fk_user_account', 'account', 'user_id', 'user', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down() {
        echo "m140708_112922_create_user cannot be reverted.\n";
        $this->dropTable('account');
        $this->dropTable('user');
        return false;
    }

}
