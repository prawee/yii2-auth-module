<?php
/*
 * 2014-07-02
 * prawee@hotmail.com
 */
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\icons\Icon;
Icon::map($this);
//use prawee\uploads\Uploadify;


$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['profile']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-3"> 
        <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNDAiIGhlaWdodD0iMTQwIj48cmVjdCB3aWR0aD0iMTQwIiBoZWlnaHQ9IjE0MCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjcwIiB5PSI3MCIgc3R5bGU9ImZpbGw6I2FhYTtmb250LXdlaWdodDpib2xkO2ZvbnQtc2l6ZToxMnB4O2ZvbnQtZmFtaWx5OkFyaWFsLEhlbHZldGljYSxzYW5zLXNlcmlmO2RvbWluYW50LWJhc2VsaW5lOmNlbnRyYWwiPjE0MHgxNDA8L3RleHQ+PC9zdmc+" style="width: 250px; height: 250px;" data-src="holder.js/250x250" class="img-thumbnail" alt="250x250">
        <?php
        /*echo Uploadify::widget([
            'model'=>$model,
        ]);*/
        ?>
    </div>
    <div class="col-xs-9">
        <?php
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute'=>'userType.name',
                    'name'=>'userType.name',
                    'label'=>'Type'
                ],
                'username',
                'email',
                'created',
                'modified',
                'last_login',
                [
                    'attribute'=>'status',
                    'name'=>'status',
                    'value'=>$model->Status,
                ]
            ],
        ]);
        ?>
        <p>
            <?= Html::a(Icon::show('edit').' Edit', ['/auth/user/update-profile', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Icon::show('refresh').' Change Password', ['/auth/user/change-password', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    </div>
</div>