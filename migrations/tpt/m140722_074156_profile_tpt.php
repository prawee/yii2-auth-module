<?php

use yii\db\Schema;
use yii\db\Migration;

class m140722_074156_profile_tpt extends Migration
{
    public function up()
    {
        $this->addColumn('user','fullname',Schema::TYPE_STRING.'(128) NOT NULL');
        $this->addColumn('user','telephone',Schema::TYPE_INTEGER);
        $this->addColumn('user','nation',Schema::TYPE_STRING.'(60) NOT NULL');
        $this->addColumn('user','address',Schema::TYPE_TEXT);
        $this->addColumn('user','organization',Schema::TYPE_STRING.'(255) NOT NULL');
        $this->addColumn('user','expired_date',Schema::TYPE_DATE);
        $this->addColumn('user','parent_id',Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m140722_074156_profile_tpt cannot be reverted.\n";

        return false;
    }
}
