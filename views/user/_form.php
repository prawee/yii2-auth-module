<?php
/*
 * 20140722
 * prawee@hotmail.com
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use common\models\UserType;
Icon::map($this);

Modal::begin([
    'id' => 'form-modal',
    'closeButton' =>false,
    'header' =>Icon::show('user').'<b>Add New User</b>']);
?>
<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>

    <?php
    $listSourceType=ArrayHelper::map(UserType::find()->asArray()->all(),'id','name');
    ?>
    <?= $form->field($model, 'user_type_id')->dropDownList($listSourceType) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>
    
    <?php if(!$model->isNewRecord):?>
    <?= $form->field($model, 'status')->dropDownList([0=>'Inactive',1=>'Active']) ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Icon::show('plus').' Create' : Icon::show('edit').' Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Icon::show('times').' Cancel', ['index'], ['class' => 'btn btn-danger'])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
Modal::end()?>