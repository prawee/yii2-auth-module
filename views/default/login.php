<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;

Icon::map($this);

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\LoginForm $model
 */
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div class="form-group">
                    <?= Html::submitButton(Icon::show('sign-in').' Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

                    <?php if(Yii::$app->getModule('auth')->register):?>
                    <?= Html::a(Icon::show('edit').' Register',['/auth/default/signup'],['class' => 'btn btn-success']) ?>
                    <?php endif; ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
