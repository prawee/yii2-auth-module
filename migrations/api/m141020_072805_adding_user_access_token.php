<?php

use yii\db\Schema;
use yii\db\Migration;

class m141020_072805_adding_user_access_token extends Migration {

    public function up() {
        $this->addColumn('user','access_token',  Schema::TYPE_STRING.'(255) null');
    }

    public function down() {
        echo "m141020_072805_adding_user_access_token cannot be reverted.\n";
        return false;
    }

}
