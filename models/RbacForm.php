<?php

/*
 * 2014-06-27
 * prawee@hotmail.com
 */

namespace auth\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;
//use auth\models\RbacForm;

class RbacForm extends ActiveRecord {

    public $parent;

    public static function tableName() {
        return Yii::$app->authManager->itemTable;
    }

    public function rules() {
        return [
            [['type', 'description', 'name', 'parent'], 'required'],
            ['parent', 'exist', 'targetAttribute' => 'name'],
            ['name', 'unique']
        ];
    }

    public function attributeLabels() {
        return [
            'name' => 'Name',
        ];
    }

    public function afterFind() {
        parent::afterFind();
        $db = new Query();
        $parent = $db
                ->select('parent')
                ->where(['child' => $this->name])
                ->from(\Yii::$app->authManager->itemChildTable)
                ->scalar();
        if ($parent) {
            $this->parent = $parent;
        }
    }

}
