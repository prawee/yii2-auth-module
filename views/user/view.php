<?php
/*
 * 20140722
 * prawee@hotmail.com
 */

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;
use kartik\icons\Icon;

Icon::map($this);

Modal::begin([
    'id' => 'form-modal',
    'closeButton' =>false,
    'header' =>Icon::show('eye') . '<b>View ('.  ucfirst($model->username).')</b>']);
?>
<div class="user-view">
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            ['attribute' => 'userType.name','label'=>'User Type'],
            'username',
            'email:email',
            'created',
            'modified',
            'last_login',
            ['attribute' => 'status','value'=>$model->Status],
        ],
    ])
    ?>
    <p>
        <?= Html::a(Icon::show('times').'Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
    </p>
</div>
<?php
Modal::end()?>