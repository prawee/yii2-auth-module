<?php
/*
 * 2014-07-21
 */
namespace auth\models;

use Yii;
use yii\base\Model;
use auth\models\User;

class ResetForm extends Model{
    public $oldPassword;
    public $password;
    public $repeatPassword;
    private $_user = false;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['oldPassword', 'password','repeatPassword'], 'required'],
            ['repeatPassword','compare','compareAttribute'=>'password','message'=>"Passwords don't match"],
            ['oldPassword', 'validatePassword'],
        ];
    }
    
    public function validatePassword() {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->oldPassword)) {
                $this->addError('oldPassword', 'Incorrect old password.');
            }
        }
    }
    
    public function resetPassword(){
        if($this->validate()){
            $user=$this->getUser();
            $user->setPassword($this->password);
            if($user->save()){
                Yii::$app->getSession()->setFlash('success','Change your password complete!.');
                return true;
            }
        }else{
            return false;
        }
    }
    
    public function getUser() {
        if ($this->_user === false && Yii::$app->user->identity->id) {
            $this->_user = User::findIdentity(Yii::$app->user->identity->id);
        }
        return $this->_user;
    }
}
