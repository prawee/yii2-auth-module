/*
 * 2014-06-23
 * @author Prawee Wongsa <prawee@hotmail.com>
 */
jQuery(document).ready(function() {
    /*for permission*/
    $("#permission-management-modal > .modal-dialog").css('width','60%');
    $("#permission-management-modal").modal({backdrop: "static", keyboard: false});
    
    /*for form page*/
    $("#form-modal > .modal-dialog").css('width','50%');
    $("#form-modal").modal({backdrop: "static", keyboard: false});
    
    /*for content dialog*/
    $("#content-modal > .modal-dialog").css('width','80%');
    $("#content-modal").modal({backdrop: "static", keyboard: false});
    
    /*for full page*/
    $("#xs-modal > .modal-dialog").css('width','90%');
    $("#xs-modal").modal({backdrop: "static", keyboard: false}); 
    
    
    /*for middle page*/
    $("#middle-modal > .modal-dialog").css('width','70%');
    $("#middle-modal").modal({backdrop: "static", keyboard: false});
    
    $('#btn-modal').click(function (){
        $('#main-modal').modal({backdrop: "static", keyboard: false})
            .find('#main-content-modal')
            .load($(this).attr('value'));
    });
});
function submitForm($form) {
    $.post(
        $form.attr("action"),
        $form.serialize()
    )
        .done(function(result) {
            $form.parent().html(result.message);
            $('#main-modal').modal('hide');
        })
        .fail(function() {
            console.log("server error");
            $form.replaceWith('<button class="newType">Fail</button>').fadeOut()
        });
    return false;
}
