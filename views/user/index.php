<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\icons\Icon;
Icon::map($this);
//use auth\models\User;
//use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <p>
        <?= Html::a(Icon::show('plus') . ' Create', ['create'], ['class' => 'btn btn-success']) ?>
        <?php if(Yii::$app->user->identity->id===1):?>
        <?= Html::a(Icon::show('th') . ' Permission Management', ['/auth/permission'], ['class' => 'btn btn-info']) ?>
        <?php endif;?>
    </p>
    <?php Pjax::begin(['id'=>'user-pjax','enablePushState' => false]);?>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'email',
            /* [
              'attribute' => 'email',
              'value' => function($data) {
              return '******';
              }
              ], */
            [
                'attribute' => 'status',
                'filter' => [0 => 'Deleted', 1 => 'Inactive', 2 => 'Active', 3 => 'Suspended'],
                'value' => function($data) {
            return $data->Status;
        }
            ],
            [
                'class' => 'prawee\grid\ActionColumn',
                'template' => '{permission}{view} {update} {delete}',
                'buttons'=>[
                    'permission'=>function($url,$data){
                        return Html::a(Icon::show('check'), ['/auth/user/permission', 'id' => $data->id], [
                            'class' => 'btn btn-xs btn-success',
                            'style' => 'margin-right:5px;',
                            'title' => 'Permission',
                            'data-pjax' => '0',
                            'data-toggle'=>'tooltip'
                        ]);
                    },
                ],
                'options'=>['style'=>'width:130px;']
            ]
        ]
    ]);?>
    <?php Pjax::end();?>
</div>