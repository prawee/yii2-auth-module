<?php

namespace auth\models;

use Yii;
use yii\web\IdentityInterface;
use common\models\User as CUser;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends CUser implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_SUSPENDED = 3;
    const TYPEID = 1;

    /**
     * @var string the raw password. Used to collect password input and isn't saved in database
     */
    public $password;
    
    private $statuses = [
        self::STATUS_DELETED => 'Deleted',
        self::STATUS_INACTIVE => 'Inactive',
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_SUSPENDED => 'Suspended',
    ];
    
    private $_isSuperAdmin = null;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'modified'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['modified'],
                ],
                'value' => function () {
            return new Expression('NOW()');
        }
            ],
        ];
    }

    public function getStatus($status = null) {
        if ($status === null) {
            return $this->statuses[$this->status];
        }
        return $this->statuses[$status];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" it is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->getSecurity()->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->getSecurity()->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /*
     * 20140603 
     */

    public function login($duration = 0) {
        return Yii::$app->user->login($this, $duration);
    }

    /*
     * 20140617
     */

    public static function tableName() {
        return Yii::$app->getModule('auth')->tableMap['User'];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            //['user_type_id','default','value'=>self::TYPEID,'on'=>'signup'],
            //['status', 'default', 'value' => self::STATUS_ACTIVE, 'on' => 'signup'],
            ['user_type_id', 'safe'],
            ['status', 'safe'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist', 'message' => 'There is no user with such email.', 'on' => 'requestPasswordResetToken'],
            ['email', 'unique', 'message' => 'This email address has already been taken.', 'on' => 'signup'],
            ['password', 'required', 'on' => ['signup','create']],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function scenarios() {
        return [
            'signup' => ['username', 'email', 'password'],
            'profile' => ['username', 'email', 'password'],
            'resetPassword' => ['password'],
            'requestPasswordResetToken' => ['email'],
            'login' => ['last_login'],
                ] + parent::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'last_login' => 'Last Visit Time',
            'created' => 'Create Time',
            'modified' => 'Update Time'
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (($this->isNewRecord || in_array($this->getScenario(), ['resetPassword', 'profile'])) && !empty($this->password)) {
                $this->password_hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            }
            if ($this->isNewRecord) {           
                $this->status = self::STATUS_ACTIVE;
                $this->auth_key=Yii::$app->getSecurity()->generateRandomString();
            }
            if($this->isNewRecord && $this->hasAttribute('user_type_id')){
                $this->user_type_id = self::TYPEID;
            }
            if ($this->getScenario() !== \yii\web\User::EVENT_AFTER_LOGIN) {
                //last_login
                $this->hasAttribute('last_login')? $this->setAttribute('last_login', new Expression('NOW()')):'';
                //updated
                $this->hasAttribute('updated')? $this->setAttribute('updated', new Expression('NOW()')):'';
            }
            return true;
        }
        return false;
    }

    /*
     * 2014-06-25
     * Returns whether the logged in user is an administrator.
     *
     * @return boolean the result.
     */

    public function getIsSuperAdmin() {
        if ($this->_isSuperAdmin !== null) {
            return $this->_isSuperAdmin;
        }

        $this->_isSuperAdmin = in_array($this->username, Yii::$app->getModule('auth')->superAdmins);
        return $this->_isSuperAdmin;
    }

}
