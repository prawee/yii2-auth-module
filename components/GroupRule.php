<?php
/*
 * 2015-01-23
 * @author Prawee Wongsa <prawee@hotmail.com>
 */
namespace auth\components;

use Yii;
use yii\rbac\Rule;
class GroupRule extends Rule{
    public $name='isGroup';
    public function execute($user, $item, $params)
    {   
        if(!Yii::$app->user->isGuest){
            $group=Yii::$app->user->identity->groupname;
            return $group===$item->name;  
        }
        return false;
    }
}
