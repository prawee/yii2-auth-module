/*
 * 2014-10-29
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 * http://www.yiiframework.com/wiki/664/activating-bootstrap-3-tooltips-popover-for-your-yii-site
 */ 
$(function(){
    $("[data-toggle='tooltip']").tooltip(); 
    $("[data-toggle='popover']").popover();
});