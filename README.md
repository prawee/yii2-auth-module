Yii2 Authentication.
====================
Yii 2 User Authentication & Role Based Access Control (RBAC) Module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist prawee/yii2-auth-module "*"
```

or add

```
"prawee/yii2-auth-module": "*"
```

above file
```
...
"repositories": [
        {
            "type": "git",
            "url": "https://bitbucket.org/prawee/yii2-auth-module.git"
        },
    ],
....
```
    


to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
main.php
<?php
....
'modules'=>[
    'auth'=>[
        'class'=>'auth\Module',
        'tableMap'=>[
            'User'=>'user'
        ],
        'modal'=>true
    ],
],
'components'=>[
    'user'=>[
        //'identityClass' => 'auth\models\User',
        //'enableAutoLogin' => true,
        //'loginUrl'=>'auth/default/login',
        'class' => 'auth\components\User',
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
    ],
],
....
?>
```

```php
params.php
return [
    ...
    'icon-framework'=>'fa',
    ...
];
```

```html
Login:              localhost/projectname/admin/auth/default/login
Logout:             localhost/projectname/admin/auth/default/logout
Signup:             localhost/projectname/admin/auth/default/signup
Reset Password:     localhost/projectname/admin/auth/resetpassword
Set Permission:     localhost/projectname/admin/auth/user
Manage Permission:  localhost/projectname/admin/auth/permission
Profile:            localhost/projectname/admin/auth/user/profile 
```

Rbac
-------
in console config file main-local.php
```php
.....
'components'=>[
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=teachdev',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ]
....
```

Migrate Database
----------------
Using command. ( Linux )
```php
./yii migrate  --migrationPath=@vendor/yiisoft/yii2/rbac/migrations
./yii migrate  --migrationPath=@auth/migrations 
./yii migrate  --migrationPath=@auth/migrations/profile
./yii migrate  --migrationPath=@auth/migrations/api
```

Using command. (Windows)
```php
yii migrate  --migrationPath=@vendor/yiisoft/yii2/rbac/migrations
yii migrate  --migrationPath=@auth/migrations 
yii migrate  --migrationPath=@auth/migrations/profile
yii migrate  --migrationPath=@auth/migrations/api
```

Initializing
------------
1.Create all models
```php
localhost/your-project/gii/model

Table Name
*

Click Preview Button
Click Generate Button
```
2.Create admin\app\models\User model extends by common\models\User
```php
localhost/your-project/admin/gii/model

namespace admin\app\models;
use auth\models\User as CUser;
class User extends CUser{}
```
3.Create admin\app\models\UserSearch by localhost/your-project/admin/gii/crud
```php
Model Class
admin\app\models\User

Search Model Class
admin\app\models\UserSearch

Controller Class
admin\app\controllers\UserController

Click Preview Button

Action Unselect all
Check UserSearch only

Click Generate Button
```

History
=======
branch 1.0.tpt 