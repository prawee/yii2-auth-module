<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use kartik\icons\Icon;

Icon::map($this);

$this->title='Signup';
?>

<?php Modal::begin(['id' => 'signup-modal','closeButton'=>[], 'header' => '<h2>'.Icon::show('edit').'Register</h2>'])?>
<div class="row">
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin([
            'id' => 'signup-form',
            'enableAjaxValidation'=>false,
            'validateOnType'=>false,
        ]); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <div class="form-group">
            <?= Html::submitButton(Icon::show('edit').' Register', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            <?= Html::a(Icon::show('times').' Cancel',Yii::$app->homeUrl,['class' => 'btn btn-danger']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end(); ?>
