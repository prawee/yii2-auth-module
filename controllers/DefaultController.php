<?php
namespace auth\controllers;

use Yii;
use yii\web\Controller;
use auth\models\LoginForm;
use auth\models\User;
use auth\Asset;

class DefaultController extends Controller{
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    } 

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function init()
    {
        parent::init();
        Asset::register($this->view);
    }
    public function actionLogin() {
        
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {       
            if (Yii::$app->getModule('auth')->modal):
                $this->layout='modal';
                $js = '$("#form-modal").modal("show")';
                $this->getView()->registerJs($js);
                return $this->render('modal', [
                            'model' => $model
                ]);
            else :         
                $this->layout=isset(Yii::$app->getModule('auth')->layoutOne)?Yii::$app->getModule('auth')->layoutOne:'//main';
                return $this->render('login', [
                            'model' => $model
                ]);
            endif;
        }
    }
    
    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    public function actionSignup(){
        $model=new User;
        $model->setScenario('signup');
        
        if($model->load(Yii::$app->getRequest()->post()) && $model->save()){
            if(Yii::$app->getUser()->login($model)){
                return $this->goHome();
            }
        }
       
        if (Yii::$app->getModule('auth')->modal):
            $this->layout='modal';
            $js = '$("#signup-modal").modal("show")';
            $this->getView()->registerJs($js);
            return $this->render('signupModal',['model'=>$model]);
        else :
            return $this->render('signup',['model'=>$model]);
        endif;
    }

}

