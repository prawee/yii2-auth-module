<?php

namespace auth\models;

use Yii;
use yii\base\Model;
use auth\models\User;

/**
 * Login form
 */
class LoginForm extends Model {

    public $username;
    public $password;
    public $rememberMe = true;
    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword() {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            
            /*echo '<br/><br/><br/><br/>';
            
            echo '<pre>';
            print_r(Yii::$app->getUser());
            echo '</pre>';
            
            echo '<pre>';
            print_r($this->getUser());
            echo '</pre>';
            
            //Yii::$app->user->login($this->getUser(),0);
            $this->getUser()->login(0);
            
            echo '<pre>';
            print_r(Yii::$app->getUser());
            echo '</pre>';*/
            //return Yii::$app->user->login($this->getUser(), $this->rememberMe ? Yii::$app->getModule('auth')->rememberMeTime : 0);
            //return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            return $this->getUser()->login($this->rememberMe?Yii::$app->getModule('auth')->rememberMeTime:0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser() {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }
        return $this->_user;
    }

}
