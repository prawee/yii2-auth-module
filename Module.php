<?php
namespace auth;

class Module extends \yii\base\Module
{
    public $controllerNamespace='auth\controllers';
    public $rememberMeTime = 2592000; // 30 days
    public $modal=false;
    
    public $tableMap=[
        'User'=>'User'
    ];
    public $superAdmins = ['admin'];
    
    /*
     * 20140720
     * on mode normal not modal
     * for main layout or alone
     */
    public $layoutOne;

    /*
     * 20150306
     * for show button on login page
     */
    public $register=true;
    
    public function init(){
        parent::init();
        $this->setAliases(['@auth'=>__DIR__]);
    }
}

