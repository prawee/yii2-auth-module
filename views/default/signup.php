<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;

Icon::map($this);

$this->title='Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin([
            'id' => 'signup-form',
            'enableAjaxValidation'=>false,
            'validateOnType'=>false,
        ]); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <div class="form-group">
            <?= Html::submitButton(Icon::show('edit').' Register', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            <?= Html::a(Icon::show('times').' Cancel',['/auth/default/login'],['class' => 'btn btn-danger']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

