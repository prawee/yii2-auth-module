<?php
/*
 * 2014-06-24
 * prawee@hotmail.com
 */
namespace auth;

use yii\web\AssetBundle;

class Asset extends AssetBundle{
    /*
     * @inheritdoc
     */
    public $sourcePath='@auth/assets';
    
    /*
     * @inheritdoc
     */
    public $css=[
        'css/modal.css',
        'css/fancytree.css',
    ];
    /*
     * @inheritdoc
     */
    public $js=[
        'js/modal.js',
        'js/tooltips.js',
    ];
    
    public $depends=[
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
