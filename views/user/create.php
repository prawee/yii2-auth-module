<?php
/*
 * 20140722
 * prawee@hotmail.com
 */
$this->title = 'Add New User';
?>
<div class="user-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>