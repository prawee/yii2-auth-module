<?php
/*
 * 20140722
 * prawee@hotmail.com
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use kartik\icons\Icon;

Icon::map($this);

$this->title = 'Update Profile';
Modal::begin([
    'id' => 'form-modal',
    'closeButton' =>[],
    'header' => '<b>' . Icon::show('refresh') . 'Update Profile</b>']);
?>
<div class="update-profile">
    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'update-profile-form']); ?>
            <?= $form->field($model, 'email') ?>
            <div class="form-group">
                <?= Html::submitButton(Icon::show('edit') . ' Save', ['class' => 'btn btn-primary', 'name' => 'update-profile-button']) ?>
                <?= Html::a(Icon::show('times') . ' Cancel',['/auth/user/profile'], ['class' => 'btn btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
Modal::end()?>