<?php
/*
 * 2014-07-21
 * prawee@hotmail.com
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use kartik\icons\Icon;

Icon::map($this);

$this->title = 'Update Profile';

Modal::begin([
    'id' => 'form-modal',
    'closeButton' =>false,
    'header' =>Icon::show('edit') . '<b>Update ('.  ucfirst($model->username).')</b>']);
?>
<div class="update-profile">
    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'update-form']); ?>
            <?= $form->field($model, 'email') ?>
            <div class="form-group">
                <?= Html::submitButton(Icon::show('edit') . ' Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
                <?= Html::a(Icon::show('times') . ' Cancel',['/auth/user/index'], ['class' => 'btn btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
Modal::end()?>