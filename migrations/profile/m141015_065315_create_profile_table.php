<?php

use yii\db\Schema;
use yii\db\Migration;

class m141015_065315_create_profile_table extends Migration
{
    public function up()
    {
        //options
        switch(Yii::$app->db->driverName){
            case 'mysql':
                $options = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
                break;
            case 'pgsql':
                $options = null;
                break;
            default:
                throw new RuntimeException('Your database is not supported!');     
        }
        $this->createTable('profile',[
            'user_id'=>  Schema::TYPE_INTEGER,
            'photo'=>  Schema::TYPE_STRING.'(40) NULL',
        ],$options);
        $this->addForeignKey('fk_user_profile', 'profile', 'user_id', 'user', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('profile');
    }
}
