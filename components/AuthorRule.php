<?php
/*
 * 2015-01-22
 * @author Prawee Wongsa <prawee@hotmail.com>
 */
namespace auth\components;

use yii\rbac\Rule;
class AuthorRule extends Rule{
    public $name='isAuthor';
    public function execute($user, $item, $params)
    {
        return isset($params[key($params)])?$params[key($params)]->user_id==$user:false;
    }
}