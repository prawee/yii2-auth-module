<?php

use yii\db\Schema;
use yii\db\Migration;

class m141010_094528_adding_user_authentication extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;
        
        /*
         * adding role
         */
        //administrator
        $admin=$auth->createRole('administrator');
        $admin->description='Administrator';
        $auth->add($admin);
        
        //member
        $member=$auth->createRole('member');
        $member->description='Member';
        $auth->add($member);
        
        /*
         * adding permission
         */
        //user-management
        $manage=$auth->createPermission('auth.user');
        $manage->description='User Management';
        $auth->add($manage);
        
        //user-index
        $list=$auth->createPermission('auth.user.index');
        $list->description='- List User';
        $auth->add($list);
        $auth->addChild($manage,$list);
        
        //user-create
        $create=$auth->createPermission('auth.user.create');
        $create->description='- Create User';
        $auth->add($create);
        $auth->addChild($manage,$create);
        
        //user-update
        $update=$auth->createPermission('auth.user.update');
        $update->description='- Update User';
        $auth->add($update);
        $auth->addChild($manage,$update);
        
        //user-delete
        $delete=$auth->createPermission('auth.user.delete');
        $delete->description='- Delete User';
        $auth->add($delete);
        $auth->addChild($manage,$delete);
        
        //user-profile
        $profile=$auth->createPermission('auth.user.profile');
        $profile->description='- View User Profile';
        $auth->add($profile);
        $auth->addChild($manage,$profile);
        
        //user-update-profile
        $updateprofile=$auth->createPermission('auth.user.update-profile');
        $updateprofile->description='- Update User Profile';
        $auth->add($updateprofile);
        $auth->addChild($manage,$updateprofile);
        
        //user-change-password
        $changepwd=$auth->createPermission('auth.user.change-password');
        $changepwd->description='- Change User Password';
        $auth->add($changepwd);
        $auth->addChild($manage,$changepwd);
        
        /*
         * Assignment
         */
        $auth->assign($admin,1);
        $auth->addChild($admin,$manage);
        $auth->addChild($member,$profile);
        $auth->addChild($member,$changepwd);
        $auth->addChild($member,$updateprofile);
        
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
