<?php
namespace auth\models;

use yii\base\Model;
use yii\db\ActiveRecord;

class AssignmentForm extends Model{
    public $userId;
    public $roleName;
    public $createAt;
    
    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
                ],
                'value' => function () {
                    return new Expression('CURRENT_TIMESTAMP');
                }
            ],
        ];
    }
    public function attributeLabels() {
        return [
            'userId'=>'',
            'roleName'=>'',
            'createAt'=>'',
        ]+parent::attributeLabels();
    }
}

